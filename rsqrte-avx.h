#pragma once
#include <x86intrin.h>
#include "rsqrte.h"

template <> inline void rsqrte<0>(const float* A, float* B, int n) {
  for (int i = 0; i < n; i += 8) {
    __m256 x = _mm256_load_ps(&A[i]);
    __m256 y = _mm256_rsqrt_ps(x);
    _mm256_store_ps(&B[i], y);
  }
}
