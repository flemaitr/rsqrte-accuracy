#pragma once

#include <cmath>

template <int NR = 0>
static inline void rsqrte(const float* A, float* B, int n) {
  if (NR < 0) {
    for (int i = 0; i < n; ++i) {
      B[i] = 1.f / std::sqrt(A[i]);
    }
    return;
  }
  rsqrte<0>(A, B, n);
  for (int i = 0; i < n; ++i) {
    float x = 0.5f * A[i];
    float y = B[i];
    for (int j = 0; j < (int) NR; ++j) {
      float a = y*y*x;
      y *= 1.5f - a;
    }
    B[i] = y;
  }
}
