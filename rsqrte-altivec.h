#pragma once
#include <altivec.h>
#include "rsqrte.h"

template <> inline void rsqrte<0>(const float* A, float* B, int n) {
  for (int i = 0; i < n; i += 4) {
    vector float x = vec_ld(0, &A[i]);
    vector float y = vec_rsqrte(x);
    vec_st(y, 0, &B[i]);
  }
}
