#pragma once
#include <x86intrin.h>
#include "rsqrte.h"

template <> inline void rsqrte<0>(const float* A, float* B, int n) {
  for (int i = 0; i < n; i += 4) {
    __m128 x = _mm_load_ps(&A[i]);
    __m128 y = _mm_rsqrt_ps(x);
    _mm_store_ps(&B[i], y);
  }
}
