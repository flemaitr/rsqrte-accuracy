#include <limits>
#include <iostream>
#include <cmath>
#include <cstdint>
#include <cstring>


#include "rsqrte.h"
#ifdef __ARM_NEON
#include "rsqrte-neon.h"
#endif
#ifdef __ALTIVEC__
#include "rsqrte-altivec.h"
#endif
#if defined(__AVX512F__)
#include "rsqrte-avx512.h"
#elif defined(__AVX__)
#include "rsqrte-avx.h"
#elif defined(__SSE__)
#include "rsqrte-sse.h"
#endif

static inline float i2f(std::uint32_t i) {
  float f;
  std::memcpy(&f, &i, 4);
  return f;
}
static inline std::uint32_t f2i(float f) {
  std::uint32_t i;
  std::memcpy(&i, &f, 4);
  return i;
}

static inline double i2f(std::uint64_t i) {
  double f;
  std::memcpy(&f, &i, 8);
  return f;
}
static inline std::uint64_t f2i(double f) {
  std::uint64_t i;
  std::memcpy(&i, &f, 8);
  return i;
}

class error {
  private:
    double ref = 0.;
    double approx = 0.;
    error() = default;

    double one_ulp() const {
      float r = ref;
      return std::nextafter(r, std::numeric_limits<float>::infinity()) - r;
    }
  public:
    error(double ref, double approx) : ref(ref), approx(approx) {}
    error(const error&) = default;
    error& operator=(const error&) = default;
    ~error() = default;

    double absolute() const {
      return std::fabs(ref - approx);
    }
    double relative() const {
      return absolute() / std::fabs(ref);
    }
    double distance() const {
      return double(std::abs(std::int64_t(f2i(ref) - f2i(approx)))) / double(1ll << (53 - 24));
    }
    double ulp() const {
      return absolute() / one_ulp();
    }

    double bits() const {
      return -std::log2(relative());
    }

    friend std::ostream& operator<<(std::ostream& out, error e) {
      out << "absolute error: " << e.absolute() << "\n";
      out << "relative error: " << e.relative() << "\n";
      out << "fake ulp error: " << e.relative() * double(1 << 23) << "\n";
      out << "true ulp error: " << e.ulp()      << "\n";
      out << "distance error: " << e.distance() << "\n";
      out << "accuracy  bits: " << e.bits()     << "\n";
      return out;
    }

    template <unsigned NR>
    static __attribute((noinline)) error max(float start, float stop) {
      error e_max(1., 1.);

      alignas(4096) std::uint32_t inI[1024];
      alignas(4096) float in[1024], out[1024];

      float x = start;
      while (x < stop) {
        std::uint32_t istart = f2i(x);
        for (int i = 0; i < 1024; ++i) {
          inI[i] = istart + i;
        }
        std::memcpy(&in, &inI, 4096);
        x = i2f(istart + 1024);
        rsqrte<NR>(in, out, 1024);
        for (int i = 0; i < 1024; ++i) {
          error e(1. / std::sqrt(double(in[i])), out[i]);
          e_max = e.relative() > e_max.relative() ? e : e_max;
        }
      }
      return e_max;
    }
};

int main() {
  std::cout << "IEEE:\n";
  std::cout << error::max<-1>(0.5f, 2.f) << std::flush;
  std::cout << "O NR:\n";
  std::cout << error::max<0>(0.5f, 2.f) << std::flush;
  std::cout << "1 NR:\n";
  std::cout << error::max<1>(0.5f, 2.f) << std::flush;
  std::cout << "2 NR:\n";
  std::cout << error::max<2>(0.5f, 2.f) << std::flush;
  std::cout << "3 NR:\n";
  std::cout << error::max<3>(0.5f, 2.f) << std::flush;
  std::cout << "4 NR:\n";
  std::cout << error::max<4>(0.5f, 2.f) << std::flush;
}
