#pragma once
#include <arm_neon.h>
#include "rsqrte.h"

template <> inline void rsqrte<0>(const float* A, float* B, int n) {
  for (int i = 0; i < n; i += 4) {
    float32x4_t x = vld1q_f32(&A[i]);
    float32x4_t y = vrsqrteq_f32(x);
    vst1q_f32(&B[i], y);
  }
}
