ARCH ?= native

rsqrte: rsqrte.cpp rsqrte.h rsqrte-neon.h rsqrte-altivec.h rsqrte-sse.h rsqrte-avx.h rsqrte-avx512.h
	$(CXX) -std=c++11 -O3 -g -march=$(ARCH) -Wall -Wextra -Wpedantic rsqrte.cpp -o rsqrte

.PHONY: rsqrte
