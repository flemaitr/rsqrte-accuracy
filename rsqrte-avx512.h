#pragma once
#include <x86intrin.h>
#include "rsqrte.h"

template <> inline void rsqrte<0>(const float* A, float* B, int n) {
  for (int i = 0; i < n; i += 16) {
    __m512 x = _mm512_load_ps(&A[i]);
#ifdef __AVX512ER__
    __m512 y = _mm512_rsqrt28_ps(x);
#else
    __m512 y = _mm512_rsqrt14_ps(x);
#endif
    _mm512_store_ps(&B[i], y);
  }
}
