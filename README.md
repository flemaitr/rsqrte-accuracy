Simple repo to test the accuracy of the rsqrte instructions on different architectures
- SSE/AVX: `_mm_rsqrt_ps`
- AVX512: `_mm512_rsqrt14_ps`
- Neon: `vrsqrteq_f32`
- Altivec/VSX: `vec_rsqrte`
